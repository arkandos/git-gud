{
  description = "Get flamed for the changes you are about to commit.";

  outputs = { self, nixpkgs }:
    with import nixpkgs { system = "x86_64-linux"; }; {
      defaultPackage.x86_64-linux = pkgs.writeShellApplication {
        name = "git-gud";

        runtimeInputs = with pkgs; [ git jq curl ];

        text = ''
          #!${stdenv.shell}
          ${builtins.readFile ./git-gud.sh}
        '';

        checkPhase = "${stdenv.shellDryRun} $target";
      };

      app.x86_64-linux = {
        type = "app";
        program = "${self.defaultPackages.x86_64-linux}/bin/git-gud";
      };

      devShell.x86_64-linux = pkgs.mkShell {
        buildInputs = with pkgs; [
          bash jq curl
        ];
      };
    };
}
