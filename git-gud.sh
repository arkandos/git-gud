#!/bin/bash

if [ -z "$OPENAI_API_KEY" ]; then
    echo "ERROR: OPENAI_API_KEY is not set. Export this environment variable and (maybe) set it in your bashrc!"
    exit 1
fi

# if [ -z "$OPENAI_ORG_ID" ]; then
#     echo "ERROR: OPENAI_ORG_ID is not set. Export this environment variable and (maybe) set it in your bashrc!"
#     exit 1
# fi

req='{
    "stream": true,
    "model": "gpt-3.5-turbo",
    "temperature": 0.4,
    "top_p": 1,
    "presence_penalty": 0,
    "frequency_penalty": 1,
    "max_tokens": 500,
    "messages": [{
        "role": "system",
        "content": "You are git-gud, a git sub-command flaming the user for their staged changes or commits, provided to you in the standard git-show format.\n\nYou respond the user with with mean insults regarding their code, and also sometimes useful code reviews, using insults, sarcasm and irony. Be as mean and sarcastic as possible.\n\nHere is the patch you should review:"
    }]
}'

if [ $# -eq 0 ]; then
    input="$(git diff)"
else
    input="$(git show $@)"
fi
if [ -z "$input" ]; then
    echo "ERROR: No changes found using 'git diff'"
    exit 1
fi

input="$(echo "$input" | cut -z -d ' ' -f 1-1500 | tr -d '\0')"

req=$(jq -c --arg input "$input" '.messages+=[{"role":"user","content":$input}]' <<< $req)

curl https://api.openai.com/v1/chat/completions \
    --silent --no-buffer \
    -H "Content-Type: application/json" \
    -H "Authorization: Bearer $OPENAI_API_KEY" \
    -d "$req" \
| while read -r line; do
    if ! [[ $line =~ ^data: ]]; then
        # echo $line
        continue
    fi
    if [ "$line" = "data: [DONE]" ]; then
        break
    fi

    data="$(echo $line | cut -c 6- | jq '.choices[0].delta.content')"
    if [ "$data" = "null" ]; then
        continue
    fi

    # JSON escapes are mostly compatible with Bash escapes, except for \", which
    # we hit a lot in code reviews. So we first use sed to handle those cases...
    data=$(sed 's/\\\"/\"/g' <<< $data)

    # ... and then we just remove the enclosing quotation marks of that JSON string,
    # and send it to echo to do the escaping!
    echo -en "${data:1:-1}"
done

